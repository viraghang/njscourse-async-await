const axios = require('axios')

const getExchangeRate = async (from, to) => {
  try {
    const res = await axios.get(`http://api.fixer.io/latest?base=${from}`)
    const rates = res.data.rates[to]

    if (rates) return rates
    throw new Error()
  } catch(e) {
    throw new Error(`Unable to get exchange rate from ${from} to ${to}`)
  }
}

// const getExchangeRate = (from, to) => {
//   return axios.get(`http://api.fixer.io/latest?base=${from}`).then(res => {
//     return res.data.rates[to]
//   })
// }

const getCountries = async currencyCode => {
  try {
    const res = await axios.get(`https://restcountries.eu/rest/v2/currency/${currencyCode}`)
    return res.data.map(country => country.name)
  } catch(e) {
    throw new Error(`Unable to get countries that use ${currencyCode}`)
  }
}

// const getCountries = currencyCode => {
//   return axios.get(`https://restcountries.eu/rest/v2/currency/${currencyCode}`).then(res => {
//     return res.data.map(country => country.name)
//   })
// }

const convertCurrency = (from, to, amount) => {
  let countries
  return getCountries(to).then(tempCountries => {
    countries = tempCountries
    return getExchangeRate(from, to)
  }).then(rate => {
    const exchangedAmount = Math.floor(amount * rate)

    return `${amount} ${from} is worth ${exchangedAmount} ${to}\nIt can be used in the following countries: ${countries.join(', ')}`
  })
}

const convertCurrencyAlt = async (from, to, amount) => {
  let countries = await getCountries(to)
  let rate = await getExchangeRate(from, to)
  const exchangedAmount = Math.floor(amount * rate)

  return `${amount} ${from} is worth ${exchangedAmount} ${to}\nIt can be used in the following countries: ${countries.join(', ')}`
}

// getExchangeRate('USD', 'CAD').then(rate => console.log(rate)).catch(e => console.log(e))
// getCountries('USD').then(countries => console.log(countries)).catch(e => console.log(e))
// convertCurrency('USD', 'CAD', 100).then(res => console.log(res)).catch(e => console.log(e))
convertCurrencyAlt('USD', 'CAD', 100).then(res => console.log(res)).catch(e => console.log(e))
